import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import About from '@/components/About'
import Axios from '@/components/Axios'
import Home from '@/components/Home'
import Register from '@/components/Register'
import Dashboard from '@/components/Dashboard'
import TodasArvores from '@/components/TodasArvores'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/axios',
      name: 'axios',
      component: Axios
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/todas-arvores',
      name: 'todas-arvores',
      component: TodasArvores
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    }
  ]
})
