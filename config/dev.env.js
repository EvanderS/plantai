'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  HOST: '"https://api-plantai.herokuapp.com"'
  // HOST: '"http://localhost:3001"'
})

