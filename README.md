# frontend

> Vue Front-End for Sisprol

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


# Arquitetura do Front-End

## build
Pasta contendo os scripts de construção da aplicação. Os scripts foram gerados automaticamente pelo webpack, e não se recomenda sua edição manual.

## config
Contém as configurações do sistema, referentes a sua execução numa máquina local ou num servidor.

## src
Pasta raiz do projeto.

### assets

### components
Componentes usados no front-end da aplicação.

### constants
Constantes usadas em vários locais da aplicação.

### router
Contém as rotas utilizadas no sistema.

## static
Arquivos que precisam ser estáticos.

## test
Scripts de teste gerados automaticamente.